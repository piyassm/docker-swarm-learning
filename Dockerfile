FROM node:14
WORKDIR /usr/src/app
COPY ["package.json", "."]
# RUN npm install
ARG NODE_ENV
RUN if [ "$NODE_ENV" = "development" ]; \
    then \
    npm install; \
    echo "is development"; \
    else \
    npm install --only=prod; \
    echo "is not development"; \
    fi
RUN echo your NODE_ENV is $NODE_ENV;
COPY . ./
ENV PORT 3000
EXPOSE $PORT

CMD ["npm", "run", "dev"]
