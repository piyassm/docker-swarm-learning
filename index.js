const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const redis = require("redis");
const session = require("express-session");

const {
  MONGO_IP,
  MONGO_PORT,
  MONGO_USER,
  MONGO_PASSWORD,
  REDIS_URL,
  REDIS_PORT,
  REDIS_SECRET,
} = require("./config/config");

const apiRouter = require("./routes/api");

let RedisStore = require("connect-redis")(session);
let redisClient = redis.createClient({ host: REDIS_URL, port: REDIS_PORT });

const app = express();

const mongoURL = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_IP}:${MONGO_PORT}/mydb?authSource=admin`;

const connectWithRetry = () => {
  mongoose
    .connect(mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    .then(() => console.log("successfully connect to MongoDB"))
    .catch((e) => {
      console.log(e);
      setTimeout(() => {
        connectWithRetry();
      }, 5000);
    });
};

connectWithRetry();

app.enable("trust proxy");
app.use(cors());
app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    cookie: {
      secure: false,
      httpOnly: true,
      maxAge: 30000,
    },
    resave: false,
    saveUninitialized: true,
    secret: REDIS_SECRET,
  })
);

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello APP");
  console.log("Run");
});

app.use("/api", apiRouter);

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`listining on port ${port}`));
