const express = require("express");
const v1Router = require("./v1");

const router = express.Router();

const index = (req, res) => {
  res.send("Hello APP API");
};

router.route("/").get(index);
router.use("/v1", v1Router);

module.exports = router;
