const express = require("express");
const postRouter = require("./postRoute");
const userRouter = require("./userRoute");
const protect = require("../../../middleware/authMiddleware");
const router = express.Router();

const index = (req, res) => {
  res.send("Hello APP API V1");
};

router.route("/").get(index);
router.use("/posts", protect, postRouter);
router.use("/user", userRouter);

module.exports = router;
